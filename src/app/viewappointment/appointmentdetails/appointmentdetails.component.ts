import { Component, OnInit } from '@angular/core';
import { ServicesService } from 'src/app/services.service';
@Component({
  selector: 'app-appointmentdetails',
  templateUrl: './appointmentdetails.component.html',
  styleUrls: ['./appointmentdetails.component.scss']
})
export class AppointmentdetailsComponent implements OnInit {

  constructor(public api:ServicesService) { }
  appointments:any;
  ngOnInit(): void {
    this.api.get().subscribe(res => {
      this.appointments = res;
      console.log(this.appointments);
    });
  }
  trackByIndex = (index:number):number =>{
    return index;
  }
}
