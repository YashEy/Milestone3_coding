import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';

import { AppointmentdetailsComponent } from './appointmentdetails.component';

describe('AppointmentdetailsComponent', () => {
  let component: AppointmentdetailsComponent;
  let fixture: ComponentFixture<AppointmentdetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports:[RouterTestingModule,
        HttpClientModule
      ],
      declarations: [ AppointmentdetailsComponent ],
      providers:[HttpClient,FormBuilder]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AppointmentdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
