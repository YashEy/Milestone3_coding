import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ViewappointmentRoutingModule } from './viewappointment-routing.module';
import { AppointmentdetailsComponent } from './appointmentdetails/appointmentdetails.component';


@NgModule({
  declarations: [
    AppointmentdetailsComponent
  ],
  imports: [
    CommonModule,
    ViewappointmentRoutingModule
  ]
})
export class ViewappointmentModule { }
