import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WelcomepageRoutingModule } from './welcomepage-routing.module';
import { HomeComponent } from './home/home.component';


@NgModule({
  declarations: [
    HomeComponent
  ],
  imports: [
    CommonModule,
    WelcomepageRoutingModule
  ]
})
export class WelcomepageModule { }
