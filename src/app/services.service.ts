import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ServicesService {

  constructor(private http: HttpClient) { }
  url : string = "http://localhost:3000/appointments";
   get() {
     return this.http.get(this.url);
   }

   post(appointment:any) {
     return this.http.post(this.url,appointment);
   }

}
