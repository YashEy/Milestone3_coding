import { TestBed } from '@angular/core/testing';

import { ProjeintercepInterceptor } from './projeintercep.interceptor';

describe('ProjeintercepInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      ProjeintercepInterceptor
      ]
  }));

  it('should be created', () => {
    const interceptor: ProjeintercepInterceptor = TestBed.inject(ProjeintercepInterceptor);
    expect(interceptor).toBeTruthy();
  });
});
