import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BookingappointmentModule } from './bookingappointment/bookingappointment.module';
import { ProjeintercepInterceptor } from './projeintercep.interceptor';
import { QueryModule } from './query/query.module';
import { ViewappointmentModule } from './viewappointment/viewappointment.module';
import { WelcomepageModule } from './welcomepage/welcomepage.module';
import { HeaderComponent } from './header/header.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BookingappointmentModule,
    QueryModule,
    ViewappointmentModule,
    WelcomepageModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass:ProjeintercepInterceptor,
      multi:true
      }
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
