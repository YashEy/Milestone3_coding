import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
export class Contact {
  constructor(
    public Fullname: string,
    public phonenumber: number,
    public email: string,
    public message: string
  ) { }
}

@Component({
  selector: 'app-contactus',
  templateUrl: './contactus.component.html',
  styleUrls: ['./contactus.component.scss']
})
export class ContactusComponent implements OnInit {

  @Output() contactdata = new EventEmitter<Contact>();
  public contactForm :any ;
  public obj: any = {};
  constructor(private fb: FormBuilder) { 
    contactForm: FormGroup ;
  }

  ngOnInit(): void {
    this.contactForm = this.fb.group({
      Fullname: ["", [Validators.required]],
      phonenumber: ["", [Validators.required]],
      email: ["", [Validators.required,Validators.pattern("[^ @]*@[^ @]*")]],
      message:["",[Validators.required]]
    });
  }
  onSubmit() {
    this.obj = { ...this.contactForm.value, ...this.obj };
    this.contactForm.value;
    console.log(
      "LOG: LoginComponent -> onSubmit -> this.contactForm.value",
      this.contactForm.value
    );

    if (this.contactForm.valid) {
      this.contactdata.emit(
        new Contact(
          this.contactForm.value.Fullname,
          this.contactForm.value.phonenumber,
          this.contactForm.value.email,
          this.contactForm.value.message
        )
      );
    }
}
aaa(){
  alert("Thank You for filling form your message is send successfully");
}

}
