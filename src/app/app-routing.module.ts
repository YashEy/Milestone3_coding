import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BookappointmentComponent } from './bookingappointment/bookappointment/bookappointment.component';
import { ContactusComponent } from './query/contactus/contactus.component';
import { AppointmentdetailsComponent } from './viewappointment/appointmentdetails/appointmentdetails.component';
import { HomeComponent } from './welcomepage/home/home.component';

const routes: Routes = [
  {path:"",component:HomeComponent},
  {path:"home",component:HomeComponent},
  {path:"bookingappointment",component:BookappointmentComponent},
  {path:"query",component:ContactusComponent},
  {path:"viewappointment",component:AppointmentdetailsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
