import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BookingappointmentRoutingModule } from './bookingappointment-routing.module';
import { BookappointmentComponent } from './bookappointment/bookappointment.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    BookappointmentComponent
  ],
  imports: [
    CommonModule,
    BookingappointmentRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class BookingappointmentModule { }
